#include "systick.h"

volatile u32	SYSTICK_TimeCount;    // 中断计数          
volatile u32	SYSTICK_Period;       // 每多少微秒进入一次中断     
volatile u32	SYSTICK_Reload;       // 滴答定时器的重载值=SYSTICK_Period * SYSTICK_UsCount 
volatile u32    SYSTICK_UsCount;      // 滴答定时器每微秒计数多少次





/**********************************************************************
* Function Name  : SysTickInit
* Description    : 初始化系统滴答
* Input          : period:多少微妙进入一次中断
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void SYSTICK_Init(u32 period)
{
	SYSTICK_TimeCount = 0;
	SYSTICK_Period = period;	
	SYSTICK_Reload = SystemCoreClock / 1000000 * period;  
	SYSTICK_UsCount = SystemCoreClock / 1000000; 

	
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);	
	SysTick_Config(SYSTICK_Reload);     
}



/**********************************************************************
* Function Name  : SYSTICK_GetTime
* Description    : 自滴答定时器开始计数以来的微秒数
* Input          : None
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
u32 SYSTICK_GetTime(void)
{	
	u32 tmp1, tmp2;
	volatile u32 val1, val2, val3;
	
	val1 = SysTick->VAL;
	tmp1 = SYSTICK_TimeCount;
	val3 = SysTick->VAL;
	
	// 如果在读取SYSTICK_TimeCount得时候进入了systick中断
	if(val3 < val1)
	{
		tmp2 = SYSTICK_TimeCount;
		val2 = SysTick->VAL;
	}
	else
	{
		tmp2 = tmp1;
		val2 = val1;
	}  
	return (u32)(tmp2 * SYSTICK_Period + (SYSTICK_Reload - val2) / SYSTICK_UsCount);
}





/**********************************************************************
* @Functionname  : SYSTICK_Delay_us
* @Brief         : 微秒级延时
* @Input         : None
* @Output        : None
* @Return        : None
* @Remark        : None
**********************************************************************/
void SYSTICK_Delay_us(volatile u32 tim)
{ 
	u32 temp; 
	temp = SYSTICK_GetTime();
	while((SYSTICK_GetTime() - temp) < tim)
	{;}
}




/**********************************************************************
* @Functionname  : SYSTICK_Delay_ms
* @Brief         : 毫秒级延时
* @Input         : None
* @Output        : None
* @Return        : None
* @Remark        : None
**********************************************************************/
void SYSTICK_Delay_ms(volatile u32 tim)
{ 
	u32 temp; 
	temp = SYSTICK_GetTime();
	while((SYSTICK_GetTime() - temp) < (tim * 1000))
	{;}
}










