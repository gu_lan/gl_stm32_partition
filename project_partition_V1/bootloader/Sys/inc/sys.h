#ifndef _SYS_H
#define _SYS_H

#include "stm32f10x.h"   
#include "sys_usart.h"
#include "systick.h"

#define Sys_Printf                 USART1_Printf

#define Sys_Delay_us               SYSTICK_Delay_us
#define Sys_Delay_ms               SYSTICK_Delay_ms
#define Sys_Get_Time               SYSTICK_GetTime

void Sys_Init(void);

void INTX_DISABLE(void);
void INTX_ENABLE(void);
void MSR_MSP(u32 addr);	//���ö�ջ��ַ

#endif //_SYS_H


