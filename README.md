# stm32_partition

#### 介绍
project_partition_V1 : 区域的划分和启动过程  
project_partition_V2 ：USART+DMA 收发不定长数据   
project_partition_V3 ：在V2的基础上增加了帧管理程序  

 
#### 安装教程

无

#### 参与贡献

1.  author:古澜
2.  email:410900747@qq.com
3.  blogs:https://www.cnblogs.com/gulan-zmc/


#### 码云特技

