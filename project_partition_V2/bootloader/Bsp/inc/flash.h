#ifndef _FLASH_H
#define _FLASH_H

#include "sys.h"
#define FLASH_PAGE_SIZE      2048

s8 EraseFlash(u32 flashaddr,u32 page);
s8 WriteFlash(u32 flashaddr, void *buff, u32 len);

#endif //_FLASH_H


