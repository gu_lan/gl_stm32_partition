#include "partition.h"
#include "flash.h"





/**********************************************************************
* Function Name  : Check_Part_Tab
* Description    : 参数表是否存在
* Input          : None
* Output         : None
* Return         : 0:存在  -1,不存在
* Remark         : None
**********************************************************************/
s8 Check_Parament_Tab(void)
{
	Parameters_Table_t *Parameters_tab;
	Parameters_tab = (Parameters_Table_t*)PARAMETERS_TAB_BASE_ADDR;
	if(Parameters_tab->parameters_flag == 0xa55a){
		return 0;
	}else{
		return -1;
	}
}







/**********************************************************************
* Function Name  : Parament_Tab_Init
* Description    : 初始化参数表
* Input          : None
* Output         : None
* Return         : 0:初始化成功   1:初始化失败
* Remark         : None
**********************************************************************/
s8 Parament_Tab_Init(void)
{
	Parameters_Table_t Parameters_tab;
	/* check_Parameters_tab 变量是用于校验数据是否写入正确 */
	Parameters_Table_t *check_Parameters_tab ;
	check_Parameters_tab = (Parameters_Table_t*)PARAMETERS_TAB_BASE_ADDR;
	Parameters_tab.parameters_flag = 0xa55a;
	Parameters_tab.app_update_flag = 0;
	Parameters_tab.boot_addr = BOOT_BASE_ADDR;
	Parameters_tab.app_address = APP_BASE_ADDR;
	Parameters_tab.app_bak_address = APP_BAK_BASE_ADDR;
	Parameters_tab.firmware_address = FIRMWARE_BASE_ADDR;
	Parameters_tab.data_exchange = 0;
	/* 擦除参数表页 */
	EraseFlash(PARAMETERS_TAB_BASE_ADDR,1);
	/* 将数据写入到参数表 */
	WriteFlash(PARAMETERS_TAB_BASE_ADDR,(void*)&Parameters_tab,sizeof(Parameters_Table_t));
	if(check_Parameters_tab->parameters_flag != 0xa55a){
		return -1;
	}
	return 0;
}




