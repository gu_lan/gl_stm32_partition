#include "sys.h"
#include "partition.h"



typedef struct FUNC
{
void (*RW_And_ZI_Init)(void);
void (*Num_Inc)(void);
void (*Num_Dec)(void);
int (*Get_Num) (void);
int (*Get_Num1) (void);
unsigned int (*Get_Num_Addr)(void);
unsigned int (*Get_Flag)(void);
int (*Swap_Num)(void *num1,void *num2,unsigned int size);
int (*My_Men_Copy)(char*desc, char *src, unsigned int len);
}FUNC_S;

FUNC_S gFunc __attribute__((at(0x0807c000)));


typedef void(*App_Fun_t)(void);
App_Fun_t app_main ;
App_Fun_t firm_fun;

int main(void)
{
	//Parameters_Table_t *check_Parameters_tab ;
	//check_Parameters_tab = (Parameters_Table_t*)PARAMETERS_TAB_BASE_ADDR;
	Sys_Init();
	//Sys_Printf("reload firmware rw \r\n");
	//
	Sys_Printf("gFunc 0x%x\r\n",&gFunc);
	Sys_Printf("RW_And_ZI_Init 0x%x\r\n",gFunc.RW_And_ZI_Init);
	Sys_Printf("Num_Inc 0x%x\r\n",gFunc.Num_Inc);
	//firm_fun = (App_Fun_t)*(vu32*)(0x0807c000);
	(gFunc.RW_And_ZI_Init)();
	/* frimware的RW和ZI段重定位 */
	//firm_fun();
	//Sys_Printf("finsh \r\n");
	/* 检查参数表 */
	if(Check_Parament_Tab()){
		USART1_Printf("Parament Tabel  inexistence\r\n");
		Parament_Tab_Init();
	}
	
//	Sys_Printf("parameters_flag = %x\r\n",check_Parameters_tab->parameters_flag);
//	Sys_Printf("app_update_flag = %x\r\n",check_Parameters_tab->app_update_flag);
//	Sys_Printf("boot_addr = %x\r\n",check_Parameters_tab->boot_addr);
//	Sys_Printf("app_address = %x\r\n",check_Parameters_tab->app_address);
//	Sys_Printf("app_bak_address = %x\r\n",check_Parameters_tab->app_bak_address);
//	Sys_Printf("firmware_address = %x\r\n",check_Parameters_tab->firmware_address);
//	Sys_Printf("data_exchange = %x\r\n",check_Parameters_tab->data_exchange);
	Sys_Printf("hello bootloader\r\n");
	Sys_Delay_ms(10);
	
	
	if(((*(vu32*)(0x0800C000+4))&0xFF000000)==0x08000000){
		
//		Sys_Printf("boot app ...\r\n");
//		Sys_Printf("0x%x\r\n",*(vu32*)(0x0800C000));
//		Sys_Printf("0x%x\r\n",*(vu32*)(0x0800C004));
		app_main = (App_Fun_t)*(vu32*)(0x0800C004);
		/* 设置中断向量表偏移 */
		SCB->VTOR = (FLASH_BASE | 0xC000);
		/* 设置应用程序的堆栈 */
	    MSR_MSP(*(vu32*)(0x0800C000));
		/* 跳转到应用程序执行 */
	    app_main();
	}else{
		Sys_Printf("boot addr error\r\n");
	}
	
	while(1);
}


