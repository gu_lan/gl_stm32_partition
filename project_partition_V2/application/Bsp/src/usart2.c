#include "usart2.h"

void (*USART2_Service)(u8 *buff, u16 len);






volatile u8 USART2_DMA_RxBuff[USART2_DMA_RXBUFFLEN];

volatile u8 USART2_DMA_TxBuff[USART2_DMA_TXBUFFLEN];








void USART2_GPIO_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    /* 初始化 USART2_TX  ----- PA2 */
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2; 
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;    
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* 初始化 USART2_RX  ----- PA3 */
    GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &GPIO_InitStructure);  
}




void USART2_Config(u32 baudrate)
{
    USART_InitTypeDef USART_InitStructure;
    USART_ClockInitTypeDef  USART_ClockInitStructure;
    /* 初始化时钟 */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); 
/************************使用异步模式，这个不用配置也可以*****************************/
    /* 关闭同步时钟(即关闭串口的同步模式; 需要使用同步模式,则使能这个) */
    USART_ClockInitStructure.USART_Clock = USART_Clock_Disable;
    /* 配置时钟稳态值为低电平(即空闲时，时钟线的电平为低电平) */
    USART_ClockInitStructure.USART_CPOL = USART_CPOL_Low;
    /* 配置时钟相位,在时钟的第二个边沿进行数据捕获 */
    USART_ClockInitStructure.USART_CPHA = USART_CPHA_2Edge;
    /* 最后一个数据位不从时钟线上输出 */
    USART_ClockInitStructure.USART_LastBit = USART_LastBit_Disable;
    /* 初始化 USART2 的同步参数，这里没有使用同步时钟 */
    USART_ClockInit(USART2, &USART_ClockInitStructure);
/************************使用异步模式，这个不用配置也可以*****************************/

    /* 设置波特率 */
    USART_InitStructure.USART_BaudRate = baudrate;
    /* 设置数据位长度为 8 */
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    /* 设置停止位是1 */
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    /* 不适用奇偶校验 */
    USART_InitStructure.USART_Parity = USART_Parity_No ;
    /* 关闭硬件流控 */
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    /* 使能 USART2 接收和接收 */
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    /* 初始化 USART2 的异步参数,在STM32中通常使用这种传输方式 */
    USART_Init(USART2, &USART_InitStructure);
}




void USART2_DMA_RX_Config(u8 *Buffer, s32 NumData) 
{
    DMA_InitTypeDef  DMA_InitStructure;
    /* 使能时钟 */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);             
    /* 将 DMA1_Channel6 设置为缺省 */
    DMA_DeInit(DMA1_Channel6);
    /* 配置外设的地址为串口的就收寄存器的地址 */
    DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART2->DR));  
    /* 设置内存的地址 */
    DMA_InitStructure.DMA_MemoryBaseAddr      = (uint32_t)(Buffer);
    /* 设置传输方向为 外设到内存 */
    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralSRC;
    /* 设置缓冲区Buffer的大小 */
    DMA_InitStructure.DMA_BufferSize          = NumData;
    /* 禁止外设的的地址自增 */
    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
    /* 使能内存地址自增 */
    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
    /* 设置外设每次传输数据的大小为字节(Byte) */
    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Byte;
    /* 设置内存每次传输数据的大小为字节(Byte) */
    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;
    /* 将设置DMA传输的模式为循环传输 */
    DMA_InitStructure.DMA_Mode                = DMA_Mode_Circular;
    /* 设置当前DMA通道的优先级为高优先级 */
    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
    /* 禁止内存到内存传输 */
    DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;
    
    /* 初始化DMA1通道6 */
    DMA_Init(DMA1_Channel6, &DMA_InitStructure);
}







void USART2_DMA_TX_Config(u8 *Buffer, s32 NumData) 
{
    DMA_InitTypeDef  DMA_InitStructure;
    /* 使能时钟 */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);             
    /* 将 DMA1_Channel7 设置为缺省 */
    DMA_DeInit(DMA1_Channel7);
    /* 配置外设的地址为串口的发送数据寄存器的地址 */
    DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART2->DR));  
    /* 设置内存的地址 */
    DMA_InitStructure.DMA_MemoryBaseAddr      = (uint32_t)(Buffer);
    /* 设置传输方向为 内存到外设 */
    DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;
    /* 设置缓冲区Buffer的大小 */
    DMA_InitStructure.DMA_BufferSize          = NumData;
    /* 禁止外设的的地址自增 */
    DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
    /* 使能内存地址自增 */
    DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
    /* 设置外设每次传输数据的大小为字节(Byte) */
    DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Byte;
    /* 设置内存每次传输数据的大小为字节(Byte) */
    DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;
    /* 将设置DMA传输的模式为正常传输 */
    DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;
    /* 设置当前DMA通道的优先级为高优先级 */
    DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
    /* 禁止内存到内存传输 */
    DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;
    /* 初始化DMA1通道7 */
    DMA_Init(DMA1_Channel7, &DMA_InitStructure);
}




void USART2_Enable(void)
{
    /* 使能串口2 */
    USART_Cmd(USART2, ENABLE);
    /* 使能串口2的DMA发送 */
    USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
    /* 使能串口2的DMA接收 */
    USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);
    /* 使能DMA1通道7 */
    DMA_Cmd(DMA1_Channel7, ENABLE); 
    /* 使能DMA1通道6 */
    DMA_Cmd(DMA1_Channel6, ENABLE); 
}






void USART2_Init(u32 baudrate)
{
    /* 初始化USART2的GPIO */
    USART2_GPIO_Config();
    /* 设置串口 */
    USART2_Config(baudrate);
    
    /* 设置USART2的发送DMA */
    USART2_DMA_TX_Config((u8 *)USART2_DMA_RxBuff, 0); 
    /* 设置USART2的接收DMA */
    USART2_DMA_RX_Config((u8 *)USART2_DMA_RxBuff, USART2_DMA_RXBUFFLEN);
    /* 使能 */
    USART2_Enable();
}





void USART2_DMA_Rx(void)
{   
    u32 pos;
    /* USART2_RX_Pos 指向环形缓冲区数据的头部 */
    static volatile u16   USART2_RX_Pos;
    
    /* DMA1_Channel6 -> CNDTR 接收到的数据查长度 */
    /* 算出pos当前的指向,缓冲区数据的尾部 */
    pos = USART2_DMA_RXBUFFLEN - (DMA1_Channel6 -> CNDTR);
    /* 当前的pos上一次的pos大 */
    if(pos > USART2_RX_Pos)
    {
        /* 算个出buff的起始地址,算出接收到数据的长度 */
        USART2_Service((u8*)USART2_DMA_RxBuff + USART2_RX_Pos, pos - USART2_RX_Pos);
        /* 改变buffer的起始指向 */
        USART2_RX_Pos = pos;
    }
    /* 如果上一次的pos比当前的pos大，说明有部分数据在前面 */
    else if(pos < USART2_RX_Pos)
    {
        /* 计算buffer的起始地址，先将后面部分的数据放入到内存管理 */
        USART2_Service((u8*)USART2_DMA_RxBuff + USART2_RX_Pos, USART2_DMA_RXBUFFLEN - USART2_RX_Pos);
        if(pos != 0)
        {
            /* 如果头部还有数据，也将头部前面的数据放入内存管理 */
            USART2_Service((u8*)USART2_DMA_RxBuff, pos);
        }
        /* 改变头部的起始指向 */
        USART2_RX_Pos = pos;
    }
    else
    {;}
}








