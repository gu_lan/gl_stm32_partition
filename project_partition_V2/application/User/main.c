#include "sys.h"
#include "usart2.h"



/* 串口接收到的数据都是在这里进行处理，具体要怎么处理就需要看需要实现什么样的逻辑了 */

void Usart2_Rx_Call_Back(u8 *buff, u16 len){
	Sys_Printf("len = %d\r\n",len);
	for(int i=0; i<len; i++){
		Sys_Printf("0x%x ",buff[i]);
	}
	Sys_Printf("\r\n");
}




typedef unsigned int (*Get_Flag_F)(void) ;
typedef  int (*Get_Num_F)(void) ;
Get_Flag_F  Get_Flag;
Get_Num_F Get_Num;
Get_Num_F Get_Num1;
int main(void)
{
	Sys_Init();
	
	
	/* 初始化USART2 的接收回调函数 */
	USART2_Service = Usart2_Rx_Call_Back ;
	
	/* 初始化USART2 */
	USART2_Init(115200);
	
	/* 调用固件区的函数 */
	Get_Flag = (Get_Flag_F)*(vu32*)0x0807c018;
	Get_Num =  (Get_Num_F)*(vu32*)0x0807c00c;
	Get_Num1 = (Get_Num_F)*(vu32*)0x0807c010;
	Sys_Printf("flag = 0x%x\r\n",Get_Flag()); 
	Sys_Printf("num = %d\r\n",Get_Num()); 
	Sys_Printf("num1 = %d\r\n",Get_Num1()); 
	
	Sys_Printf("hello app\r\n");
	while(1){
		//
		/* 注意了，如果使用该方式接收数据，就不能使用这种延时函数，来进行长时间的延时了 */
		//Sys_Delay_ms(1000);
		
		/* 不断查询DMA接收到的数据 */
		USART2_DMA_Rx();
		
		
	}
}


