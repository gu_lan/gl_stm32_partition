#include "fun.h"
#include <stdlib.h>
int test_num = 10;
int test_num1;
unsigned int flag;
void RW_And_ZI_Init (void)
{
	//volatile unsigned int *flag = (volatile unsigned int *)(0x2000FFFF-8); //将内部的sram的最后四个字节用来记录重定位的标志
	if(flag!=0xf55faa55){
		
		extern unsigned char Image$$ER_IROM1$$Limit;
		extern unsigned char Image$$RW_IRAM1$$Base;
		extern unsigned char Image$$RW_IRAM1$$RW$$Limit;
		extern unsigned char Image$$RW_IRAM1$$ZI$$Limit;
		unsigned char * psrc, *pdst, *plimt;

		psrc  = (unsigned char *)&Image$$ER_IROM1$$Limit;;

		pdst  = (unsigned char *)&Image$$RW_IRAM1$$Base;
		plimt = (unsigned char *)&Image$$RW_IRAM1$$RW$$Limit;
		while(pdst < plimt){
		    *pdst++ = *psrc++;
		}
		psrc  = (unsigned char *)&Image$$RW_IRAM1$$RW$$Limit;
		plimt = (unsigned char *)&Image$$RW_IRAM1$$ZI$$Limit;
		while(psrc < plimt){
			*psrc++ = 0;
		}
		flag=0xf55faa55;
	}
	
	
}     

void Num_Inc(void)
{
    test_num++;
}
void Num_Dec (void)
{
	test_num--;
}
int Get_Num (void)
{
	return test_num;
}

int Get_Num1 (void)
{
	return test_num1;
}

unsigned int Get_Num_Addr(void)
{
	return (unsigned int)&test_num;
}

unsigned int Get_Flag()
{
    //volatile unsigned int *flag = (volatile unsigned int *)(0x2000FFFF-8);
	return flag;
}


int Swap_Num(void *num1,void *num2,unsigned int size)
{
	if(num1 == (void*)0 || num2 == (void*)0){
		return -1;
	}
	if(size == 1){
		unsigned char temp = *(char*)num1;
		*(unsigned char*)num1 = *(unsigned char*)num2;
		*(unsigned char*)num2 = temp;
	}else if(size == 2){
		unsigned short temp = *(short*)num1;
		*(unsigned short*)num1 = *(unsigned short*)num2;
		*(unsigned short*)num2 = temp;
	}else if(size == 4){
		unsigned int temp = *(int*)num1;
		*(unsigned int*)num1 = *(unsigned int*)num2;
		*(unsigned int*)num2 = temp;
	}else {
		return -1;
	}
	return 0;
}




int My_Men_Copy(char*desc, char *src, unsigned int len)
{
	
	unsigned int i=0;
	if(desc == (void*)0 || src == (void*)0){
		return -1;
	}
	for(i=0; i<len && desc != (void*)0 && src != (void*)0  ; i++, desc++, src++){
		*desc = *src;
	}
	return 0;
}



