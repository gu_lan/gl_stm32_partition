#include "frame.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"






/**********************************************************************
* Function Name  : Frame_Buffer_Init
* Description    : 初始化帧内存管理
* Input          : frame       : 帧管理结构体
				   len         : 缓存长度
				   validate_cb : 回调函数(主要由用户处理业务逻辑) 
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void Frame_Buffer_Init(FrameBufferStr *frame, u16 len, FRAME_VALIDATE validate_cb)
{
	/* 但是要值得注意启动文件的堆内存的大小(默认是 512Byte)。*/
	/* 如果使用大于等于512Byte的大小，则需要修改堆内存大小，否则会报 Fault 异常 */
	/* 由于本实验需要的缓存需要3Kb,所以已经在启动文件里面修改为4KB */
	frame->buffer = (u8*)malloc(len);  
	frame->BUFFERLENGTH = len;
	frame->ValidateFrame = validate_cb;
	Frame_Buffer_Clean(frame);
}









/**********************************************************************
* Function Name  : Frame_Buffer_Clean
* Description    : 清除帧内存管理
* Input          : frame       : 帧管理结构体
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void Frame_Buffer_Clean(FrameBufferStr *frame)
{
	memset(frame->buffer, 0, frame->BUFFERLENGTH);
	frame->head = 0;
	frame->count = 0;

}








/**********************************************************************
* Function Name  : Append_Frame_Buffer
* Description    : 帧内存管理
* Input          : frame       : 帧管理结构体
                   input       : 压入的数据的地址
                   length      : 压入数据的长度
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void Append_Frame_Buffer(FrameBufferStr *frame, u8 *input, u16 length)
{ 
    u16 i = 0;
    //s16 head=0;
    u16 mlen=0;
    
    // 如果上传数据长度比缓存总长度还要长
    if (length > frame->BUFFERLENGTH)
    {
        // 此处可添加错误代码
        return;
    }
    /* 需要将后面的数据copy到前面 */
    if((u16)frame->count + length > frame->BUFFERLENGTH)
    {
        /* frame->head 记录frame->buffer的头部,frame->count记录接收到数据的尾部*/
        memmove(frame->buffer, frame->buffer+frame->head, frame->count - frame->head);
        /* 重新计算接收到数据的尾部 */
        frame->count = frame->count - frame->head;
        /* 头部指向为0 */
        frame->head = 0;    
        /* 将后面的数据清零 */
        memset(frame->buffer + frame->count, 0, frame->BUFFERLENGTH - frame->count);
        /* 后面需要添加的数据超出了fream的范围 */
        if((frame->count + length) > frame->BUFFERLENGTH)
        {
            // index out of buffer range
            /* 将数据清零 */
            frame->count=0;
            frame->head=0;
            return;
        }
    }

    if(length != 0)
    {
        /* 复制后面添加进来的数据 */
        memcpy(frame->buffer+frame->count, input, length);
        frame->count+=length;
    }

    i = 0;
    
    while(frame->head + i < frame->count)
    {
        /* 调用回调函数,解析接收数据,一个一个往下迭代，直到找到需要校验的起始头部 */
        mlen = frame->ValidateFrame(frame->buffer + frame->head + i, frame->count - frame->head - i);
        
        if(mlen > 0)
         {
             /* 返回的长度比buffer的总长度长，也将frame初始化 */
            if(mlen > frame->BUFFERLENGTH)
            {
                frame->head = 0;
                frame->count = 0;   
                memset(frame->buffer, 0, frame->BUFFERLENGTH);
                break;
            }
            /* 处理完这帧数据了,将数据清零 */
            frame->head = frame->head + i + mlen;       
            /* 如果计算完的后的frame->head后比 frame->count大，说明接收的数据有偏差*/
            if(frame->head > frame->count)
            {
                frame->count = frame->head;
            }
            i = 0;
            
            if(frame->head == frame->count)
            {
                /* 有偏差则需要重新将frame->buffer初始化 */
                frame->head = 0;
                frame->count = 0;   
                memset(frame->buffer, 0, frame->BUFFERLENGTH);
                break;
            }
        }
        else
        {
            i++;    
        }       
    }

}



