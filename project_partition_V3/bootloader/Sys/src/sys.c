#include "sys.h"

void Sys_Init(void)
{
	USART1_Init(115200);
	SYSTICK_Init(1);
	
}

//关闭所有中断
void INTX_DISABLE(void)
{		  
	__ASM volatile("cpsid i");
}

//开启所有中断
void INTX_ENABLE(void)
{
	__ASM volatile("cpsie i");		  
}


__asm void MSR_MSP(u32 addr) 
{
    MSR MSP, r0 			//set Main Stack value
    BX r14
}


void SoftReset(void)
{
    __set_FAULTMASK(1); // 关闭所有中断
    NVIC_SystemReset(); // 复位
}




