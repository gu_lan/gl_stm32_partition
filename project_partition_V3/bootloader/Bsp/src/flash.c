#include "flash.h"




/**********************************************************************
* @Fuctionname   : EraseFlash
* @Brief         : 擦除Flash
* @Input         : flashaddr擦除的页地址; page:擦除的页数
* @Output        : None
* @Return        : 0:擦除完成;    -1:擦除失败
* @Remark        : None
**********************************************************************/
s8 EraseFlash(u32 flashaddr,u32 page)
{
	u32 i;
	s8 err = 0;
	FLASH_Status FLASHStatus = FLASH_COMPLETE;
	
	FLASH_Unlock();

    for(i=0; (i<page) && (FLASHStatus == FLASH_COMPLETE); i++)
	{
		FLASHStatus = FLASH_ErasePage((uint32_t)(flashaddr + i * FLASH_PAGE_SIZE));
		if(FLASHStatus != FLASH_COMPLETE)
		{
			err = -1;
		}		
	}
	FLASH_Lock();	
	return err;
}





/**********************************************************************
* @Fuctionname   : IAP_WriteFlash
* @Brief         : 写flash
* @Input         : flashaddr写数据的起始地址; buff:要写的数据; len:数据长度
* @Output        : None
* @Return        : 0:写入完成;    -1:写入失败
* @Remark        : None
**********************************************************************/
s8 WriteFlash(u32 flashaddr, void *buff, u32 len)
{
	u32 i;
	u8 * bufftemp;
	s8 err = 0;
	
	FLASH_Status FLASHStatus = FLASH_COMPLETE;
	
	FLASH_Unlock();

	bufftemp = (u8 *)buff;
	
  for(i=0; i<len; i=i+4)
	{
		FLASHStatus = FLASH_ProgramWord((uint32_t)(flashaddr + i), *(uint32_t *)(bufftemp + i));
		if(FLASHStatus != FLASH_COMPLETE)
		{
			err = -1;
		}		
	}
	FLASH_Lock();	
	return err;
}



