#ifndef _USART2_H
#define _USART2_H

#include "sys.h"



#define USART2_DMA_RXBUFFLEN 128
#define USART2_DMA_TXBUFFLEN 128
/* 帧管理程序的长度 3KB */
#define USART2_FRAME_BUFFER_LEN 3*1024

extern void (*USART2_Service)(u8 *buff, u16 len);

void USART2_GPIO_Config(void);
void USART2_Init(u32 baudrate);
void USART2_Config(u32 baudrate);
void USART2_DMA_RX_Config(u8 *Buffer, s32 NumData);
void USART2_DMA_TX_Config(u8 *Buffer, s32 NumData);
void USART2_Enable(void);

void USART2_DMA_Rx(void);

void USART2_DMA_Callback(u8 *buff, u16 len);
void USART2_Frame_Init(FRAME_VALIDATE frame_cb);

#endif



