#include "sys.h"
#include "usart2.h"

/**********************************************************************
* Function Name  : Protocol_Handle
* Description    : 帧管理的回调函数，主要是处理协议的
* Input          : None
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
/* 这个函数就是处理协议的回调函数了，具体怎么处理就留给用户来设计了 */
/* 在本实验中,这里只是做一个接收复位指令，然后执行软件复位 */
uint16_t Protocol_Handle(u8 *buf, u16 len)
{
	/* 判断头是否正确 */
	if(buf[0] == 0xa5 && buf[1] == 0xa5){
		/* 等待接收够一帧数据 */
		/* 这个 len 是包括头部的，不是协议上的len*/
		if(len >= 8){
			
			/* 判断校验位 */
			uint16_t check = (uint16_t)(buf[len-2]<<8 | buf[len-1]);
			if(check == 1){
				/* 判断是否是软复位指令 */
				if(buf[2]  == 0 && buf[3] == 1){
					/* 执行软复位 */
					SoftReset();
				}
			}
			return len;
		}
	}
	return 0;
}



typedef unsigned int (*Get_Flag_F)(void) ;
typedef  int (*Get_Num_F)(void) ;
Get_Flag_F  Get_Flag;
Get_Num_F Get_Num;
Get_Num_F Get_Num1;
int main(void)
{
	Sys_Init();
	
	
	/* 初始化 USART2 帧管理 */
	USART2_Frame_Init(Protocol_Handle);
	/* 初始化USART2 */
	USART2_Init(115200);
	
	/* 调用固件区的函数 */
	Get_Flag = (Get_Flag_F)*(vu32*)0x0807c018;
	Get_Num =  (Get_Num_F)*(vu32*)0x0807c00c;
	Get_Num1 = (Get_Num_F)*(vu32*)0x0807c010;
	Sys_Printf("flag = 0x%x\r\n",Get_Flag()); 
	Sys_Printf("num = %d\r\n",Get_Num()); 
	extern uint8_t __heap_limit;
	extern uint8_t __heap_base;
	Sys_Printf(" heap zise %d \r\n", (uint32_t)(&__heap_limit) - (uint32_t)(&__heap_base));
	Sys_Printf("num1 = %d\r\n",Get_Num1()); 
	Sys_Printf("hello app\r\n");
	while(1){
		//
		/* 注意了，如果使用该方式接收数据，就不能使用这种延时函数，来进行长时间的延时了 */
		//Sys_Delay_ms(1000);
		
		/* 不断查询DMA接收到的数据 */
		USART2_DMA_Rx();
		
		
	}
}


