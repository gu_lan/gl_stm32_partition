#ifndef _FRAME_H
#define _FRAME_H
#include "sys.h"

typedef uint16_t (*FRAME_VALIDATE)(u8 *buf, u16 len);


typedef struct
{
    u8 *buffer;//指向一块buff，用于存储一帧的数据
    u16 head;  //数据存储在头部的位置
    u16 count; //接收到的数据
    u16 BUFFERLENGTH; //buffer的最大长度
    FRAME_VALIDATE ValidateFrame; //这个是一个回调函数的指针，主要是处理帧的回掉函数
} FrameBufferStr;

void Frame_Buffer_Init(FrameBufferStr *frame, u16 len, FRAME_VALIDATE validate_cb);
void Append_Frame_Buffer(FrameBufferStr *frame, u8 *input, u16 length);
void Frame_Buffer_Clean(FrameBufferStr *frame);
#endif








