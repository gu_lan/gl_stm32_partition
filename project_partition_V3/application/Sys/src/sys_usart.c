#include <stdio.h>
#include <stdarg.h>
#include "sys_usart.h"



u8 Sys_TX_Buff[SYS_TX_BUFF_LEN];
u16 Sys_Tx_Cnt;







/**********************************************************************
* Function Name  : USART1_GPIO_Config
* Description    : 串口的GPIO口初始化
* Input          : None
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_GPIO_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(USART1_TX_CLK|USART1_RX_CLK, ENABLE);
	
	GPIO_InitStructure.GPIO_Pin   = USART1_TX_PIN; 
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;	//复用推挽输出
	GPIO_Init(USART1_TX_GPIO, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin  = USART1_RX_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(USART1_RX_GPIO, &GPIO_InitStructure);  
}








/**********************************************************************
* Function Name  : USART1_Config
* Description    : 串口1初始化
* Input          : 波特率
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_Config(u32 baudrate)
{
	USART_InitTypeDef USART_InitStructure;
	USART_ClockInitTypeDef  USART_ClockInitStructure;
	
	RCC_APB2PeriphClockCmd(USART1_CLK, ENABLE); 
	
	USART_ClockInitStructure.USART_Clock = USART_Clock_Disable;
	USART_ClockInitStructure.USART_CPOL = USART_CPOL_Low;
	USART_ClockInitStructure.USART_CPHA = USART_CPHA_2Edge;
	USART_ClockInitStructure.USART_LastBit = USART_LastBit_Disable;
	
	USART_ClockInit(USART1, &USART_ClockInitStructure);

	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	
	USART_Init(USART1, &USART_InitStructure);
}









/**********************************************************************
* Function Name  : USART1_DMA_TX_Config
* Description    : 初始化 USART1 DMA发送
* Input          : Buffer : 发送buf,  NumData : buf的长度
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_DMA_TX_Config(u8* Buffer, s32 NumData) 
{
	DMA_InitTypeDef  DMA_InitStructure;

	RCC_AHBPeriphClockCmd(USART1_DMA_TX_CLK, ENABLE);             

	DMA_DeInit(USART1_DMA_TX_CHANNEL);
	DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART1->DR));	
	DMA_InitStructure.DMA_MemoryBaseAddr      = (uint32_t)(Buffer);
	DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralDST;
	DMA_InitStructure.DMA_BufferSize          = NumData;
	DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode                = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;

	DMA_Init(USART1_DMA_TX_CHANNEL, &DMA_InitStructure);
}









/**********************************************************************
* Function Name  : USART1_DMA_RX_Config
* Description    : 初始化USART1的DMA接收
* Input          : Buffer:接收的buf,buf的长度
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_DMA_RX_Config(u8* Buffer, s32 NumData) 
{
	DMA_InitTypeDef  DMA_InitStructure;

	RCC_AHBPeriphClockCmd(USART1_DMA_RX_CLK, ENABLE);             

	DMA_DeInit(USART1_DMA_RX_CHANNEL);
	DMA_InitStructure.DMA_PeripheralBaseAddr  = (uint32_t)(&(USART1->DR));	
	DMA_InitStructure.DMA_MemoryBaseAddr      = (uint32_t)(Buffer);
	DMA_InitStructure.DMA_DIR                 = DMA_DIR_PeripheralSRC;
	DMA_InitStructure.DMA_BufferSize          = NumData;
	DMA_InitStructure.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc           = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode                = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority            = DMA_Priority_High;
	DMA_InitStructure.DMA_M2M                 = DMA_M2M_Disable;

	DMA_Init(USART1_DMA_RX_CHANNEL, &DMA_InitStructure);
}










/**********************************************************************
* Function Name  : USART1_Init
* Description    : 初始化串口1
* Input          : baudrate:波特率
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_Init(u32 baudrate)
{
	
	USART1_GPIO_Config();
	USART1_Config(baudrate);
	
	USART1_DMA_TX_Config(Sys_TX_Buff, SYS_TX_BUFF_LEN);
	//USART1_DMA_RX_Config(USART1_DMA_RxBuff, USART1_DMA_RXBUFFLEN);
	USART1_Enable();
}






/**********************************************************************
* Function Name  : None
* Description    : None
* Input          : None
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_Enable(void)
{
	/* Enable USART1 */
	USART_Cmd(USART1, ENABLE);
//	DMA_ITConfig(USART1_DMA_TX_CHANNEL, DMA_IT_TC, ENABLE);
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);
//	USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(USART1_DMA_TX_CHANNEL, ENABLE);	
//	DMA_Cmd(USART1_DMA_RX_CHANNEL, ENABLE);	
}










/**********************************************************************
* Function Name  : None
* Description    : None
* Input          : None
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_Disable(void)
{
	DMA_Cmd(USART1_DMA_TX_CHANNEL, DISABLE);
//	DMA_Cmd(USART1_DMA_RX_CHANNEL, DISABLE);
//	DMA_ITConfig(USART1_DMA_TX_CHANNEL, DMA_IT_TC, DISABLE);
	USART_DMACmd(USART1, USART_DMAReq_Tx, DISABLE);
	//USART_DMACmd(USART1, USART_DMAReq_Rx, DISABLE);
	/* Disable USART1 */
	USART_Cmd(USART1, DISABLE);		
} 









/**********************************************************************
* Function Name  : Sys_Printf
* Description    : 打印函数
* Input          : format:格式化字符串,  ...:可变参数
* Output         : None
* Return         : None
* Remark         : None
**********************************************************************/
void USART1_Printf(const char *format, ...)
{
	va_list args;
	/* 等待发送完 */
	while(DMA_GetFlagStatus(USART1_DMA_TX_FLAG)==RESET);
	va_start(args, format);
	Sys_Tx_Cnt = vsnprintf((char*)Sys_TX_Buff, SYS_TX_BUFF_LEN, (char*)format, args);
	va_end(args);
	/* 设置发送的数据 */
	DMA_Cmd(USART1_DMA_TX_CHANNEL, DISABLE);	
	DMA_SetCurrDataCounter(USART1_DMA_TX_CHANNEL,Sys_Tx_Cnt);
	DMA_Cmd(USART1_DMA_TX_CHANNEL, ENABLE);	
	/* 清除发送完成标志 */
	DMA_ClearFlag(DMA1_FLAG_TC4);
	
}

