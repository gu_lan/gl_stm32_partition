#include "sys.h"

void Sys_Init(void)
{
	/* 设置中断向量表的偏移,app程序运行地址是0x080C000 */
	USART1_Init(115200);
	SYSTICK_Init(1);
	
}


void SoftReset(void)
{
    __set_FAULTMASK(1); // 关闭所有中断
    NVIC_SystemReset(); // 复位
}






